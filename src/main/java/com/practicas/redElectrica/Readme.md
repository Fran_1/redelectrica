# Trabajo Práctico
> Introducción: Red Eléctrica 

## Descripción del escenario 
1. Tenemos aparatos que se encienden y se apagan
2. Cada aparato tiene un consumo cuando esta encendido (apagado no consume nada)
3. Una red tiene un conjunto de aparatos enchufados
4. Cada red puede proporcionar un máximo de energía
5. Una red puede tener distintos sistemas de seguridad
6. Un sistema de seguridad, al activarse, ha de mirar si el consumo de la red es mayor al soportado. Si es así, ha de apagar los aparatos necesarios para volver a los límites de seguridad
7. Puede haber distintas implementaciones de sistemas de seguridad (que apaguen primero los más potentes, que apaguen primero los menos potentes, etc...)
8. El sistema no puede apagar aparatos críticos (un tipo especial de aparato). Estos solo se pueden apagar manualmente
9. El sistema de seguridad tiene una alarma
10. Puede haber muchos tipos de alarma (que envían SMS, que hacen sonar un aparato, etc...). Todos funcionan igual: se activan y se desactivan
11. Si el sistema de seguridad no puede bajar el consumo por debajo del nivel soportado, ha de activar el sistema de alarma
12. El sistema de alarma no se alimenta de la red (no tiene un consumo, como los aparatos)