package com.practicas.redElectrica.dominio.v02;

class Aparato {
	
	public static int ENCENDIDO = 1;
	public static int APAGADO = 0;
	public static int SUSPENDIDO = 2;
	
	private int estado;
	private double consumo;
	
	public void encender() {
		System.out.println("Encendiendo...");
		this.consumo = 50;
		this.estado = Aparato.ENCENDIDO;
	}

	public void apagar() {
		System.out.println("Encendiendo...");
		this.consumo = 0;
		this.estado = Aparato.APAGADO;
	}

	public boolean isEncendido() {
		return encendido;
	}

	public void setEncendido(boolean encendido) {
		this.encendido = encendido;
	}

	public double getConsumo() {
		return consumo;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}
	
	
}
