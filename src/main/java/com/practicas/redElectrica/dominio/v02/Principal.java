package com.practicas.redElectrica.dominio.v02;

class Principal {

	public static void main(String[] args) {
		Aparato aparato = new Aparato();
		
		System.out.println(aparato.isEncendido());
		
		aparato.setEncendido(true);
		System.out.println(aparato.isEncendido());
		
		aparato.setEncendido(false);
		System.out.println(aparato.isEncendido());
		
		Red red = new Red();
		red.getAparatos().add(aparato);
	}

}
