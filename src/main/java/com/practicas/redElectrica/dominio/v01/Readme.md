# Trabajo Práctico
> Introducción: Red Eléctrica 

## Vamos a resolver: 
1. Tenemos aparatos que se encienden y se apagan
2. Cada aparato tiene un consumo cuando esta encendido **(apagado no consume nada)**

> El consumo cuando esta apagado es 0, y cuando esta encendido consume el consumo del aparato, por lo cual tenemos un **consumo actual** que depende de si esta encendido o no.
	
3. Una red tiene un conjunto de aparatos enchufados

> Si la red tiene un conjunto de aparatos enchufados, tendra un consumo que depende de la cantidad de aparatos y si estos estan encendidos o no.
