package com.practicas.redElectrica.dominio.v01;

class Aparato {
	
	private boolean encendido;
	private double consumo;
	
	public Aparato() {
		super();
	}

	public Aparato(double consumo) {
		super();
		this.consumo = consumo;
	}

	public Aparato(boolean encendido, double consumo) {
		super();
		this.encendido = encendido;
		this.consumo = consumo;
	}

	public boolean isEncendido() {
		return encendido;
	}

	public void setEncendido(boolean encendido) {
		if(encendido) {
			System.out.println("Encendiendo...");
		}
		else {
			System.out.println("Apagando...");
		}
		this.encendido = encendido;
	}

	public double getConsumo() {
		return consumo;
	}

	public void setConsumo(double consumo) {
		this.consumo = consumo;
	}
}
