package com.practicas.redElectrica.dominio.v01;

import java.util.LinkedList;

class Principal {

	public static void main(String[] args) {

		// Aqui estamos instanciando un objeto aparato
		Aparato aparato = new Aparato(60);

		// Le enviamos un mesaje al aparato para saber si esta encendido
		System.out.println("Encendido: " + aparato.isEncendido());

		// Le enviamos un mensaje al aparato para que se encienda
		aparato.setEncendido(true);
		System.out.println("Encendido: " + aparato.isEncendido());
		if (aparato.isEncendido()) {
			System.out.println("Consumo actual: " + aparato.getConsumo());
		} else {
			System.out.println("Consumo actual: " + 0);
		}

		// le enviamos un mensaje al aparato para que se apague
		aparato.setEncendido(false);
		System.out.println("Encendido: " + aparato.isEncendido());
		if (aparato.isEncendido()) {
			System.out.println("Consumo actual: " + aparato.getConsumo());
		} else {
			System.out.println("Consumo actual: " + 0);
		}

		// Creamos una red electrica
		Red red = new Red();

		// 1er intento de agregar un aparato a la red
//		red.getAparatos().add(aparato);

		// 2do intento de agregar un aparato a la red
		LinkedList aparatos = new LinkedList();
		red.setAparatos(aparatos);
		red.getAparatos().add(aparato);

		// 3er intento de agregar un aparato a la red
//		LinkedList aparatos = new LinkedList();
//		aparatos.add(aparato);
//		red.setAparatos(aparatos);

		// Consumo actual de la red
		double consumoDeLaRed = 0;
		for (Object obj : red.getAparatos()) {
			Aparato a = (Aparato) obj;
			if (a.isEncendido()) {
				consumoDeLaRed += a.getConsumo();
			}
		}
		System.out.println("Consumo actual de la red: " + consumoDeLaRed);
	}

}
